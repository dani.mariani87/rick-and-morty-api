import 'isomorphic-fetch';
import { MinimumCharacter } from '../domain/Characters';
import { restApiRetrieveAllCharacters } from './RestApiCharactersRepository';

describe('RestApiCharactersRepository - Integration test', () => {
  describe('when retrieving all characters', () => {
    it('on retrieval success', () => {
      const retrieveAll = restApiRetrieveAllCharacters();

      return retrieveAll().then(result => {
        expect(result.list.length).toBe(20);
        result.list.forEach(el => expectIsACharacter(el));
      });
    });
  });

  const expectIsACharacter = (character: MinimumCharacter): void => {
    expect(character.id).toEqual(expect.any(Number));
    expect(character.registry.name).toEqual(expect.any(String));
  };
});
