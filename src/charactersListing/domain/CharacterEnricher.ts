import { RetrieveLocationByUrl } from './LocationsRepository';
import { EnrichedCharacter, FullCharacterLocation, MinimumCharacter, MinimumCharacterLocation } from './Characters';

export type EnrichCharacter = (character: MinimumCharacter) => Promise<EnrichedCharacter>;

export const enrichCharacterWithFullInformation = (retrieveLocationByUrl: RetrieveLocationByUrl): EnrichCharacter =>
  async (minimumCharacter) => ({
    type: 'ENRICHED',
    id: minimumCharacter.id,
    avatar: minimumCharacter.avatar,
    appearsInEpisodes: minimumCharacter.appearsInEpisodes,
    origin: await retrieveLocationOrNull(retrieveLocationByUrl, minimumCharacter.origin),
    lastKnownLocation: await retrieveLocationOrNull(retrieveLocationByUrl, minimumCharacter.lastKnownLocation),
    registry: {
      name: minimumCharacter.registry.name,
      status: minimumCharacter.registry.status,
      type: minimumCharacter.registry.type,
      gender: minimumCharacter.registry.gender,
      species: minimumCharacter.registry.species,
    },
  });

const retrieveLocationOrNull = (retrieveLocationByUrl: RetrieveLocationByUrl, location: MinimumCharacterLocation | null): Promise<FullCharacterLocation | null> =>
  location
    ? retrieveLocationByUrl(location.url).catch(() => null)
    : Promise.resolve(null);
