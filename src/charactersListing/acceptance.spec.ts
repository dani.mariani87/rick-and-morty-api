import 'isomorphic-fetch';

import { init } from './index';

describe('Characters Listing Acceptance Test', () => {
  let node: HTMLElement;

  beforeEach(() => {
    node = document.createElement('div');
    document.body.appendChild(node);
  });

  afterEach(() => {
    document.body.removeChild(node);
  });

  it('should retrieve and render characters cards', () => {
    return init(node)
      .then(() => {
        verifyListIsRendered();
        verifyCharactersCardsAreRendered();
      });
  });

  const verifyListIsRendered = () => expect(node.querySelector('[data-test="characters-listing"]')).not.toBeNull();
  const verifyCharactersCardsAreRendered = () => expect(node.querySelectorAll('[data-test="character-card"]').length).toEqual(20);
});
