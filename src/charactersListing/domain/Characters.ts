
export type Character =
  | MinimumCharacter
  | EnrichedCharacter;

export type MinimumCharacter = {
  type: 'MINIMUM';
  id: number;
  registry: CharacterRegistry;
  avatar: CharacterAvatar;
  lastKnownLocation: MinimumCharacterLocation | null;
  appearsInEpisodes: Array<{ url: string }>;
  origin: MinimumCharacterLocation | null;
};

export type EnrichedCharacter = {
  type: 'ENRICHED';
  id: number;
  registry: CharacterRegistry;
  avatar: CharacterAvatar;
  lastKnownLocation: FullCharacterLocation | null;
  appearsInEpisodes: Array<{ url: string }>;
  origin: FullCharacterLocation | null;
};

export type CharacterRegistry = {
  name: string;
  species: string;
  type: string | null;
  status: CharacterStatus;
  gender: CharacterGender;
};

export type FullCharacterLocation = {
  id: number;
  name: string;
  type: string;
  dimension: string;
  residentsCount: number;
};

export type CharacterEpisode = { url: string };
export type CharacterAvatar = { url: string; };
export type MinimumCharacterLocation = { url: string; name: string; };

export type CharacterStatus = 'Alive' | 'Dead' | 'unknown';
export const aliveStatus = (): CharacterStatus => 'Alive';
export const deadStatus = (): CharacterStatus => 'Dead';
export const unknownStatus = (): CharacterStatus => 'unknown';

export type CharacterGender = 'Female'| 'Male' | 'Genderless' | 'unknown';
export const femaleGender = (): CharacterGender => 'Female';
export const maleGender = (): CharacterGender => 'Male';
export const genderlessGender = (): CharacterGender => 'Genderless';
export const unknownGender = (): CharacterGender => 'unknown';
