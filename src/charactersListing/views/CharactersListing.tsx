import * as React from 'react';

import "./styles/_characters-listing.scss";

import { CharacterCard } from './CharacterCard';
import { Character } from '../domain/Characters';
import { PlaceholderCard } from './Placeholders';

type Props = {
  isLoading: boolean;
  characters: Array<Character>;
};

export const CharactersListing: React.FC<Props> = (props) => (
  <div className="characters-listing" data-test="characters-listing">
    {
      props.isLoading
        ? renderLoader()
        : renderResults(props.characters)
    }
  </div>
);

const renderLoader = (): React.ReactElement => (
  <ul className="characters-listing__list">
    <li className="characters-listing__list-item"><PlaceholderCard /></li>
    <li className="characters-listing__list-item"><PlaceholderCard /></li>
    <li className="characters-listing__list-item"><PlaceholderCard /></li>
  </ul>
);

const renderResults = (characters: Array<Character>): React.ReactElement => (
  <ul className="characters-listing__list">
    {
      characters.map(item => (
        <li className="characters-listing__list-item" key={ item.id }>
          <CharacterCard character={ item }/>
        </li>
      ))
    }
  </ul>
);
