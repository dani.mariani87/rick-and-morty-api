import * as React from 'react';

import { Character, FullCharacterLocation } from '../domain/Characters';

import './styles/_character-card.scss';
import { TextPlaceholder } from './Placeholders';

type Props = { character: Character };

export const CharacterCard: React.FC<Props> = (props) => (
  <article className="character-card" data-test="character-card">
    <div className="character-card__avatar-container">
      <img className="character-card__avatar" src={ props.character.avatar.url } alt={ props.character.registry.name } />
    </div>

    <div className="character-card__info-container">
      <CardSection>
        <h1 className="character-card__name">{ props.character.registry.name }</h1>

        {
          props.character.registry.type
            ? <Line label="Type" value={ props.character.registry.type } />
            : null
        }

        <Line label="Status" value={ props.character.registry.status } />
        <Line label="Gender" value={ props.character.registry.gender } />
        <Line label="Species" value={ props.character.registry.species } />
      </CardSection>

      {
        props.character.origin
          ? <CardSection title="Origin">
            {
              props.character.type === 'MINIMUM'
                ? <LoadingLocation />
                : <FullLocation location={ props.character.origin } />
            }
          </CardSection>
          : null
      }

      {
        props.character.lastKnownLocation
          ? <CardSection title="Last known location">
            {
              props.character.type === 'MINIMUM'
                ? <LoadingLocation />
                : <FullLocation location={ props.character.lastKnownLocation } />
            }
          </CardSection>
          : null
      }
    </div>
  </article>
);

const CardSection: React.FC<{ title?: string }> = (props) => (
  <div className="character-card__section">
    { props.title ? (<p className="character-card__section-title">{ props.title }</p>) : null }
    { props.children }
  </div>
);

const Line: React.FC<{ label: string; value: string }> = (props) => (
  <p className="character-card__line">
    <span className="character-card__label">{ props.label }</span>
    <span className="character-card__value">{ props.value }</span>
  </p>
);

const LoadingLocation: React.FC = () => (
  <div className="character-card__location">
    <TextPlaceholder />
    <TextPlaceholder />
    <TextPlaceholder />
    <TextPlaceholder />
  </div>
);

const FullLocation: React.FC<{ location: FullCharacterLocation }> = (props) => (
  <div className="character-card__location">
    <Line label="Name" value={ props.location.name } />
    <Line label="Type" value={ props.location.type } />
    <Line label="Dimension" value={ props.location.dimension } />
    <Line label="Residents count" value={ `${ props.location.residentsCount }` } />
  </div>
);
