import { init } from './charactersListing';

import './index.scss';

const listingContainer = document.getElementById('app');

init(listingContainer!)
  // "console.logs" are just for the exercise :)
  .then(() => console.log('EVERYTHING IS OK!'))
  .catch(e => console.error('SOMETHING IS NOT WORKING!', e));
