import { loadCharacters } from './domain/LoadCharactersUseCase';
import { reactUpdateView } from './infrastructure/ReactCharactersListingView';
import { enrichCharacterWithFullInformation } from './domain/CharacterEnricher';
import { cachedRetrieveLocationByUrl } from './infrastructure/CachedLocationsRepository';
import { restApiRetrieveLocationByUrl } from './infrastructure/RestApiLocationsRepository';
import { restApiRetrieveAllCharacters } from './infrastructure/RestApiCharactersRepository';

export const init = (container: HTMLElement): Promise<void> => {

  const executeUseCase = loadCharacters(
    reactUpdateView(container!),
    restApiRetrieveAllCharacters(),
    enrichCharacterWithFullInformation(
      cachedRetrieveLocationByUrl(restApiRetrieveLocationByUrl(), new Map())
    ),
  );

  return executeUseCase();
};
