import { MinimumCharacter } from '../domain/Characters';
import { toCharacter } from './RestApiCharacterAdapter';

describe('RestApiCharacterAdapter', () => {

  it('adapts a character dto - (1)', () => {
    const actual = toCharacter({
      id: 1,
      name: 'Name',
      species: 'Species',
      origin: { name: 'Origin', url: 'origin-url' },
      status: 'Alive',
      gender: 'Female',
      location: { name: 'Location', url: 'location-url' },
      image: 'picture-url',
      episode: ['episode-1', 'episode-2'],
      type: 'Type',
    });

    expect(actual).toEqual(
      aCharacter({
        type: 'MINIMUM',
        id: 1,
        registry: {
          name: 'Name',
          species: 'Species',
          status: 'Alive',
          gender: 'Female',
          type: 'Type',
        },
        origin: { url: 'origin-url', name: 'Origin' },
        lastKnownLocation: { url: 'location-url', name: 'Location' },
        avatar: { url: 'picture-url' },
        appearsInEpisodes: [{ url: 'episode-1' }, { url: 'episode-2' }],
      }),
    );
  });

  it('adapts a character dto - (2)', () => {
    const actual = toCharacter({
      id: 2,
      name: 'Name 2',
      species: 'Species 2',
      origin: { name: 'Origin', url: '' },
      status: 'UNKNOWN',
      gender: 'UNKNOWN',
      location: { name: 'Location', url: '' },
      image: 'picture-url',
      episode: ['episode-1'],
      type: '',
    });

    expect(actual).toEqual(
      aCharacter({
        type: 'MINIMUM',
        id: 2,
        registry: {
          name: 'Name 2',
          species: 'Species 2',
          status: 'unknown',
          gender: 'unknown',
          type: null,
        },
        origin: null,
        lastKnownLocation: null,
        avatar: { url: 'picture-url' },
        appearsInEpisodes: [{ url: 'episode-1' }],
      }),
    );
  });

  const aCharacter = (character: MinimumCharacter) => character;
});
