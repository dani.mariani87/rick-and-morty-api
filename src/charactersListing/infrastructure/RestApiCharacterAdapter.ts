import { RestCharacterDTO, RestCharacterMinimumLocationDTO } from './RestApiCharactersRepository';
import { aliveStatus, MinimumCharacter, CharacterAvatar, CharacterEpisode, CharacterGender, MinimumCharacterLocation, CharacterStatus, deadStatus, femaleGender, genderlessGender, maleGender, unknownGender, unknownStatus } from '../domain/Characters';

export const toCharacter = (json: RestCharacterDTO): MinimumCharacter => ({
  type: 'MINIMUM',
  id: json.id,
  registry: {
    name: json.name,
    type: toType(json.type),
    gender: toGender(json.gender),
    status: toStatus(json.status),
    species: json.species,
  },
  origin: toLocation(json.origin),
  lastKnownLocation: toLocation(json.location),
  avatar: toAvatar(json.image),
  appearsInEpisodes: json.episode.map(toEpisode),
});

const toType = (type: string): string | null => type !== '' ? type : null;
const toAvatar = (url: string): CharacterAvatar => ({ url: url });

const toGender = (gender: string): CharacterGender => {
  switch (gender) {
    case 'Female': return femaleGender();
    case 'Male': return maleGender();
    case 'Genderless': return genderlessGender();
    case 'unknown': return unknownGender();
    default: return unknownGender();
  }
};

const toStatus = (status: string): CharacterStatus => {
  switch (status) {
    case 'Alive': return aliveStatus();
    case 'Dead': return deadStatus();
    case 'unknown': return unknownStatus();
    default: return unknownStatus();
  }
};

const toLocation = (location: RestCharacterMinimumLocationDTO): MinimumCharacterLocation | null => {
  return !location.url
    ? null
    : {
      url: location.url,
      name: location.name,
    };
};

const toEpisode = (url: string): CharacterEpisode => ({ url: url });
