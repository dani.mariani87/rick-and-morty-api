import 'isomorphic-fetch';
import { FullCharacterLocation } from '../domain/Characters';
import { restApiRetrieveLocationByUrl } from './RestApiLocationsRepository';

describe('RestApiLocationsRepository - Integration test', () => {
  describe('when retrieving a location by url', () => {
    it('on retrieval success', () => {
      const retrieveById = restApiRetrieveLocationByUrl();

      return retrieveById('https://rickandmortyapi.com/api/location/1').then(result => {
        expectIsALocation(result);
      });
    });

    it('on location not found', () => {
      const retrieveById = restApiRetrieveLocationByUrl();

      return retrieveById('https://rickandmortyapi.com/api/location/ASDFASDFSDAF')
        .then(fail)
        .catch(e => expect(e).toEqual(new Error('Not Found')));
    });
  });

  const expectIsALocation = (location: FullCharacterLocation): void => {
    expect(location.id).toEqual(expect.any(Number));
    expect(location.name).toEqual(expect.any(String));
    expect(location.residentsCount).toEqual(expect.any(Number));
  };
});
