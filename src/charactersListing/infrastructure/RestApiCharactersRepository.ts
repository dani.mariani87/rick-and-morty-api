import { toCharacter } from './RestApiCharacterAdapter';
import { RetrieveAllCharacters, RetrieveAllCharactersResult } from '../domain/CharactersRepository';

type CharactersResponseDTO = {
  results: Array<RestCharacterDTO>;
};

export type RestCharacterDTO = {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  image: string;
  episode: Array<string>;
  origin: RestCharacterMinimumLocationDTO;
  location: RestCharacterMinimumLocationDTO;
};

export type RestCharacterMinimumLocationDTO = {
  name: string;
  url: string;
};

export const restApiRetrieveAllCharacters = (): RetrieveAllCharacters =>
  () => fetch('https://rickandmortyapi.com/api/character')
    .then(response => response.json())
    .then((content: CharactersResponseDTO) => toResult(content));

const toResult = (json: CharactersResponseDTO): RetrieveAllCharactersResult => ({
  list: json.results.map(toCharacter),
});
