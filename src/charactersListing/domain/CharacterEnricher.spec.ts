import { MinimumCharacterBuilder, EnrichedCharacterBuilder, FullCharacterLocationBuilder } from '../__testBuilders/CharactersBuilders';

import { FullCharacterLocation } from './Characters';
import { RetrieveLocationByUrl } from './LocationsRepository';
import { enrichCharacterWithFullInformation } from './CharacterEnricher';

describe('CharacterEnricher', () => {

  it('when all enriched data is retrieved with success', () => {
    const originUrl = 'character-origin-url';
    const locationUrl = 'character-location-url';

    const character = MinimumCharacterBuilder.aCharacter()
      .withId(1)
      .withOriginUrl(originUrl)
      .withLocationUrl(locationUrl)
      .build();

    const enrichCharacter = enrichCharacterWithFullInformation(
      inMemoryRetrieveLocationByUrl([
        { url: originUrl, location: FullCharacterLocationBuilder.aFullLocation().withId(1).build() },
        { url: locationUrl, location: FullCharacterLocationBuilder.aFullLocation().withId(2).build() },
      ]),
    );

    return enrichCharacter(character)
      .then(enrichedCharacter => {
          expect(enrichedCharacter).toEqual(EnrichedCharacterBuilder.aCharacter().withId(1)
            .withOrigin(FullCharacterLocationBuilder.aFullLocation().withId(1).build())
            .withLocation(FullCharacterLocationBuilder.aFullLocation().withId(2).build())
            .build());
        },
      );
  });

  it('when enriched data could not be retrieved', () => {
    const originUrl = 'character-origin-url';

    const character = MinimumCharacterBuilder.aCharacter()
      .withId(1)
      .withOriginUrl(originUrl)
      .withNoLocationUrl()
      .build();

    const enrichCharacter = enrichCharacterWithFullInformation(
      inMemoryRetrieveLocationByUrl([
        { url: 'another-location', location: FullCharacterLocationBuilder.aFullLocation().withId(1).build() },
      ]),
    );

    return enrichCharacter(character)
      .then(enrichedCharacter => {
          expect(enrichedCharacter).toEqual(EnrichedCharacterBuilder.aCharacter().withId(1)
            .withNoOrigin()
            .withNoLocation()
            .build());
        },
      );
  });

  const inMemoryRetrieveLocationByUrl = (locations: Array<{ url: string; location: FullCharacterLocation }>): RetrieveLocationByUrl =>
    (url) => new Promise((resolve, reject) => {
      const found = locations.find(location => location.url === url);

      if (found) {
        return resolve(found.location);
      }

      return reject(new Error('Not found'));
    });
});
