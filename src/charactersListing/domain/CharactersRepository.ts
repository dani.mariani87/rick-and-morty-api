import { MinimumCharacter } from './Characters';

export type RetrieveAllCharactersResult = { list: Array<MinimumCharacter> };
export type RetrieveAllCharacters = () => Promise<RetrieveAllCharactersResult>;
