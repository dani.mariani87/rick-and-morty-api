import { EnrichedCharacter, FullCharacterLocation, MinimumCharacter, MinimumCharacterLocation } from '../domain/Characters';

export class MinimumCharacterBuilder {
  private id: number;
  private name: string;
  private origin: MinimumCharacterLocation | null;
  private location: MinimumCharacterLocation | null;

  static aCharacter(): MinimumCharacterBuilder {
    return new MinimumCharacterBuilder();
  }

  private constructor() {
    this.id = 1;
    this.name = '::Name::';
    this.origin = { url: 'origin-url', name: 'Origin' };
    this.location = { url: 'location-url', name: 'Location' };
  }

  withId(id: number): this {
    this.id = id;
    return this;
  }

  withOriginUrl(url: string): this {
    this.origin!.url = url;
    return this;
  }

  withLocationUrl(url: string): this {
    this.location!.url = url;
    return this;
  }

  withNoLocationUrl(): this {
    this.location = null;
    return this;
  }

  build(): MinimumCharacter {
    return {
      type: 'MINIMUM',
      id: this.id,
      registry: {
        name: this.name,
        species: 'Species',
        status: 'Alive',
        gender: 'Female',
        type: 'Type',
      },
      origin: this.origin,
      lastKnownLocation: this.location,
      avatar: { url: 'picture-url' },
      appearsInEpisodes: [{ url: 'episode-1' }],
    };
  }
}


export class EnrichedCharacterBuilder {
  private id: number;
  private name: string;
  private origin: FullCharacterLocation | null;
  private location: FullCharacterLocation | null;

  static aCharacter(): EnrichedCharacterBuilder {
    return new EnrichedCharacterBuilder();
  }

  private constructor() {
    this.id = 1;
    this.name = '::Name::';
    this.origin = FullCharacterLocationBuilder.aFullLocation().withId(1).build();
    this.location = FullCharacterLocationBuilder.aFullLocation().withId(2).build();
  }

  withId(id: number): this {
    this.id = id;
    return this;
  }

  withOrigin(origin: FullCharacterLocation): this {
    this.origin = origin;
    return this;
  }

  withLocation(location: FullCharacterLocation): this {
    this.location = location;
    return this;
  }

  withNoOrigin(): this {
    this.origin = null;
    return this;
  }

  withNoLocation(): this {
    this.location = null;
    return this;
  }

  build(): EnrichedCharacter {
    return {
      type: 'ENRICHED',
      id: this.id,
      registry: {
        name: this.name,
        species: 'Species',
        status: 'Alive',
        gender: 'Female',
        type: 'Type',
      },
      origin: this.origin,
      lastKnownLocation: this.location,
      avatar: { url: 'picture-url' },
      appearsInEpisodes: [{ url: 'episode-1' }],
    };
  }
}

export class FullCharacterLocationBuilder {
  private id: number;

  static aFullLocation() {
    return new FullCharacterLocationBuilder();
  }

  private constructor() {
    this.id = 1;
  }

  withId(id: number): this {
    this.id = id;
    return this;
  }

  build(): FullCharacterLocation {
    return {
      id: this.id,
      name: 'Location',
      dimension: 'Dimension',
      residentsCount: 1,
      type: 'Type',
    };
  }
}