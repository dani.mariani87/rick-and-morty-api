import { EnrichCharacter } from './CharacterEnricher';
import { RetrieveAllCharacters } from './CharactersRepository';
import { charactersEnriched, charactersRetrieved, UpdateView, viewInitialized } from './CharactersListingView';

type LoadCharactersUseCase = () => Promise<void>;

export const loadCharacters = (
  updateView: UpdateView,
  retrieveAll: RetrieveAllCharacters,
  enrichCharacter: EnrichCharacter,
): LoadCharactersUseCase =>
  () => Promise.resolve()
    .then(() => updateView(viewInitialized()))
    .then(() => retrieveAll())
    .then(executeAndReturnInput(result => updateView(charactersRetrieved(result.list))))
    .then(result => Promise.all(result.list.map(character => enrichCharacter(character))))
    .then(enriched => updateView(charactersEnriched(enriched)));

const executeAndReturnInput = <I, O>(fn: (input: I) => O): (input: I) => I =>
  (input: I) => {
    fn(input);
    return input;
  };
