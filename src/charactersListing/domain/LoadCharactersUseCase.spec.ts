import { MinimumCharacterBuilder, EnrichedCharacterBuilder } from '../__testBuilders/CharactersBuilders';

import { EnrichCharacter } from './CharacterEnricher';
import { loadCharacters } from './LoadCharactersUseCase';
import { RetrieveAllCharacters } from './CharactersRepository';
import { EnrichedCharacter, MinimumCharacter } from './Characters';
import { CharactersViewUpdate, UpdateView } from './CharactersListingView';

describe('LoadCharactersUseCase', () => {

  it('on success retrieving results', () => {
    const viewUpdates = aListOfViewUpdates([]);

    const characters = aListOfCharacters([
      MinimumCharacterBuilder.aCharacter().withId(1).build(),
      MinimumCharacterBuilder.aCharacter().withId(2).build(),
    ]);

    const enrichedCharacters = [
      EnrichedCharacterBuilder.aCharacter().withId(1).build(),
      EnrichedCharacterBuilder.aCharacter().withId(2).build(),
    ];

    const updateView = noOpUpdateView(viewUpdates);
    const retrieveAll = inMemoryRetrieveAllCharacters(characters);
    const enrich = inMemoryEnrichCharacter(enrichedCharacters);
    const useCase = loadCharacters(updateView, retrieveAll, enrich);

    return useCase().then(() => {
      expect(viewUpdates).toEqual(
        aListOfViewUpdates([
          { type: 'VIEW_INITIALIZED' },
          { type: 'CHARACTERS_RETRIEVED', characters: characters },
          { type: 'CHARACTERS_ENRICHED', characters: enrichedCharacters },
        ]),
      );
    })
  });

  const aListOfViewUpdates = (updates: Array<CharactersViewUpdate>) => updates;
  const aListOfCharacters = (characters: Array<MinimumCharacter>) => characters;

  const noOpUpdateView = (viewUpdates: Array<CharactersViewUpdate>): UpdateView =>
    (update) => { viewUpdates.push(update) };

  const inMemoryRetrieveAllCharacters = (characters: Array<MinimumCharacter>): RetrieveAllCharacters =>
    () => Promise.resolve({ list: characters });

  const inMemoryEnrichCharacter = (characters: Array<EnrichedCharacter>): EnrichCharacter =>
    (character) => {
      const found = characters.find(entry => entry.id === character.id);

      if (found) {
        return Promise.resolve(found);
      }

      return Promise.reject(new Error('Cannot enrich character'));
    };
});
