import { cacheAsyncOperation } from './AsyncOperationCache';
import { FullCharacterLocation } from '../domain/Characters';
import { RetrieveLocationByUrl } from '../domain/LocationsRepository';

type Cache = Map<string, Promise<FullCharacterLocation>>;

export const cachedRetrieveLocationByUrl = (delegate: RetrieveLocationByUrl, cache: Cache): RetrieveLocationByUrl =>
  (url) => cacheAsyncOperation(cache)({ key: url, operation: () => delegate(url) });
