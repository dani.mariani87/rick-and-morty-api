const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const MODE = process.env.NODE_ENV === 'production'
  ? 'production'
  : 'development';

const IS_DEVELOPMENT = MODE === 'development';

console.log('<<< WEBPACK - mode: >>>', MODE);

module.exports = {
  mode: MODE,
  entry: './src/index.ts',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: '/node_modules',
      },
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        exclude: '/node_modules',
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          IS_DEVELOPMENT
            ? 'style-loader'
            : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
    ],
  },
  devServer: {
    contentBase: './dist',
    overlay: true,
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './demo/index.html' }),
    new MiniCssExtractPlugin({ filename: 'style.css', chunkFilename: '[name].css' }),
  ],
};
