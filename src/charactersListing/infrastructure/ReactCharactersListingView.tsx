import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { UpdateView } from '../domain/CharactersListingView';
import { CharactersListing } from '../views/CharactersListing';

export const reactUpdateView = (node: HTMLElement): UpdateView => {
  const renderView = renderViewOnNode(node);

  return (update) => {
    switch (update.type) {
      case 'VIEW_INITIALIZED':
        renderView(<CharactersListing isLoading={ true } characters={ [] } />);
        break;
      case 'CHARACTERS_RETRIEVED':
        renderView(<CharactersListing isLoading={ false } characters={ update.characters } />);
        break;
      case 'CHARACTERS_ENRICHED':
        renderView(<CharactersListing isLoading={ false } characters={ update.characters } />);
        break;
      default: ensureCompleteMatch(update);
    }
  }
};

const renderViewOnNode = (node: HTMLElement) => (view: React.ReactElement): void => {
  ReactDOM.render(view, node);
};

const ensureCompleteMatch = (x: never): never => x;
