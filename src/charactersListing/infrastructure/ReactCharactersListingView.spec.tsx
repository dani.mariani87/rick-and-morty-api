import { MinimumCharacterBuilder, EnrichedCharacterBuilder } from '../__testBuilders/CharactersBuilders';

import { reactUpdateView } from './ReactCharactersListingView';
import { charactersEnriched, charactersRetrieved, viewInitialized } from '../domain/CharactersListingView';

describe('ReactCharactersListingView', () => {
  let node: HTMLElement;

  beforeEach(() => {
    node = document.createElement('div');
    document.body.appendChild(node);
  });

  afterEach(() => {
    document.body.removeChild(node);
  });

  it('on view initialization', () => {
    const updateView = reactUpdateView(node);

    updateView(viewInitialized());

    expectListingElementIsRendered();
  });

  it('on results received', () => {
    const updateView = reactUpdateView(node);

    updateView(viewInitialized());

    updateView(charactersRetrieved([
      MinimumCharacterBuilder.aCharacter().withId(1).build(),
      MinimumCharacterBuilder.aCharacter().withId(2).build(),
    ]));

    expectListingElementIsRendered();
    expect(getAllCharactersCardsElements().length).toBe(2);
  });

  it('on results enriched', () => {
    const updateView = reactUpdateView(node);

    updateView(viewInitialized());

    updateView(charactersEnriched([
      EnrichedCharacterBuilder.aCharacter().withId(1).build(),
      EnrichedCharacterBuilder.aCharacter().withId(2).build(),
    ]));

    expectListingElementIsRendered();
    expect(getAllCharactersCardsElements().length).toBe(2);
  });

  const expectListingElementIsRendered = () =>
    expect(getListingElement()).not.toBeNull();

  const getListingElement = () =>
    node.querySelector('[data-test="characters-listing"]');

  const getAllCharactersCardsElements = () =>
    getListingElement()!.querySelectorAll('[data-test="character-card"]');
});
