import * as React from 'react';

import './styles/_placeholders.scss';

export const TextPlaceholder: React.FC = () => (
  <div className="text-placeholder" />
);

export const AvatarPlaceholder: React.FC = () => (
  <div className="avatar-placeholder" />
);

export const PlaceholderCard: React.FC = () => (
  <div className="card-placeholder">
    <AvatarPlaceholder />
    <TextPlaceholder />
    <TextPlaceholder />
    <TextPlaceholder />
  </div>
);
