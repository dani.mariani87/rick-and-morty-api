
module.exports = (api) => {
  const env = api.env();

  console.log('<<< BABEL - env: >>>', env);

  return {
    presets: [
      [
        '@babel/env',
        {
          useBuiltIns: 'entry',
          corejs: '3',
          targets: targets(env),
        }
      ],
      '@babel/react',
    ],
  };
};

const targets = (env) =>
  env === 'test'
    ? { node: 'current' }
    : { browsers: 'defaults' };
