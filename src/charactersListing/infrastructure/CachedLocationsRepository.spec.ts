import { FullCharacterLocationBuilder } from '../__testBuilders/CharactersBuilders';

import { FullCharacterLocation } from '../domain/Characters';
import { cachedRetrieveLocationByUrl } from './CachedLocationsRepository';

describe('CachedLocationsRepository', () => {

  it('caches successful requests', () => {
    const cache = new Map<string, Promise<FullCharacterLocation>>();

    let locationUrl = 'URL_1';
    let location = FullCharacterLocationBuilder.aFullLocation().build();

    const delegate: jest.Mock<Promise<FullCharacterLocation>, Array<string>> = jest.fn()
      .mockImplementationOnce(() => Promise.resolve(location));

    const cachedRetrieve = cachedRetrieveLocationByUrl(delegate, cache);

    return cachedRetrieve(locationUrl)
      .then(result => expect(result).toEqual(location))
      .then(() => cachedRetrieve(locationUrl))
      .then(() => {
        expect(delegate).toHaveBeenNthCalledWith(1, locationUrl);
        expect(cache.get(locationUrl)).toEqual(Promise.resolve(FullCharacterLocationBuilder.aFullLocation().build()));
      });
  });

  it('does not cache failing requests', () => {
    const cache = new Map<string, Promise<FullCharacterLocation>>();

    let locationUrl = 'URL_1';
    let delegateError = new Error('Not retrieved');

    const delegate: jest.Mock<Promise<FullCharacterLocation>, Array<string>> = jest.fn()
      .mockImplementation(() => Promise.reject(delegateError));

    const cachedRetrieve = cachedRetrieveLocationByUrl(delegate, cache);

    return cachedRetrieve(locationUrl)
      .catch(() => cachedRetrieve(locationUrl))
      .catch(e => {
        expect(e).toEqual(delegateError);
        expect(delegate).toHaveBeenNthCalledWith(2, locationUrl);
        expect(cache.get(locationUrl)).toBeUndefined();
      });
  });
});
