
export type CacheRequest<K, V> = { key: K; operation: () => Promise<V> };

export const cacheAsyncOperation = <K, V>(cache: Map<K, Promise<V>>) =>
  (request: CacheRequest<K, V>): Promise<V> => {
    const foundInCache = cache.get(request.key);

    if (foundInCache) {
      return foundInCache;
    }

    const actualApiRequest = request.operation();

    cache.set(request.key, actualApiRequest);

    return actualApiRequest.catch(e => {
      cache.delete(request.key);
      throw e;
    });
  };
