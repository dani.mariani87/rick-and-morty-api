import { FullCharacterLocation } from './Characters';

export type RetrieveLocationByUrl = (url: string) => Promise<FullCharacterLocation>;
