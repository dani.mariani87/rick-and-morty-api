import { FullCharacterLocation } from '../domain/Characters';
import { RetrieveLocationByUrl } from '../domain/LocationsRepository';

export type RestLocationDTO = {
  id: number;
  name: string;
  type: string;
  dimension: string;
  residents: Array<string>;
  url: string;
  created: string;
};

export const restApiRetrieveLocationByUrl = (): RetrieveLocationByUrl =>
  (url) => fetch(url)
    .then(response => response.json())
    .then((content: RestLocationDTO) => toResult(content))
    .catch(() => Promise.reject(new Error('Not Found')));

const toResult = (json: RestLocationDTO): FullCharacterLocation => ({
  id: json.id,
  name: json.name,
  type: json.type,
  dimension: json.dimension,
  residentsCount: json.residents.length,
});
