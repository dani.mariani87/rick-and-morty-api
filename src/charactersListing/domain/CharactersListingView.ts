import { EnrichedCharacter, MinimumCharacter } from './Characters';

export type CharactersViewUpdate =
  | ViewInitialized
  | CharactersRetrieved
  | CharactersEnriched;

export type ViewInitialized = { type: 'VIEW_INITIALIZED' };
export type CharactersRetrieved = { type: 'CHARACTERS_RETRIEVED'; characters: Array<MinimumCharacter> };
export type CharactersEnriched = { type: 'CHARACTERS_ENRICHED'; characters: Array<EnrichedCharacter> };

export const viewInitialized = (): ViewInitialized =>
  ({ type: 'VIEW_INITIALIZED' });

export const charactersRetrieved = (characters: Array<MinimumCharacter>): CharactersRetrieved =>
  ({ type: 'CHARACTERS_RETRIEVED', characters: characters });

export const charactersEnriched = (characters: Array<EnrichedCharacter>): CharactersEnriched =>
  ({ type: 'CHARACTERS_ENRICHED', characters: characters });

export type UpdateView = (update: CharactersViewUpdate) => void;
